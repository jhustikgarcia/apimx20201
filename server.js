var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var requestJson = require('request-json');
var urlRaizMLab = "https://api.mlab.com/api/1/databases/jcontreras/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;


//var urlClientes = "https://api.mlab.com/api/1/databases/jcontreras/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  /*res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");*/
  //res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//para proteger pass
const Crypto = require('node-crypt');
const crypto = new Crypto({
  key: 'b95d8cb128734ff8821ea634dc34334535afe438524a782152d11a5248e71b01',
  hmacKey: 'dcf8cd2a90b1856c74a9f914abbb5f467c38252b611b138d8eedbe2abb4434fc'
});

//los datos a enp
const unencryptedValue = 'your secret value';

// Encrypt it
const encryptedValue = crypto.encrypt(unencryptedValue);
// Decrypt it
const decryptedValue = crypto.decrypt(encryptedValue);
//should(decryptedValue).eql(unencryptedValue);


var movimientosJSON = require('./movimientosv2.json');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req,res){
  //res.send("hola mundo");
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.get('/v1/movimientos', function(req,res){
  //res.send("hola mundo");
  res.sendfile('movimientosv1.json');
})

app.get('/v2/movimientos', function(req,res){
  //res.send("hola mundo");
  res.json(movimientosJSON);
})

app.get('/v2/movimientos/:indice', function(req,res){
  //res.send("hola mundo");
  res.json(movimientosJSON[req.params.indice]);
})

app.get('/v3/movimientosquery', function(req,res){
  console.log(res.query);
  res.send("recibido")
  //res.send("hola mundo");
  //res.json(movimientosJSON[req.query]);
})

app.post('/v3/movimientos', function(req,res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("Movimiento dado de alta");
})

app.get('/Cambio', function(req,res){
  clienteMLabRaiz = requestJson.createClient("https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718/datos/oportuno");
  clienteMLabRaiz.headers['Bmx-Token'] = '0291667af6649bdc41cbb0576dd92114181f57e44624581a5973e36ea4f97347';
  //console.log("vamos a obtener el tipo de cambio")

  clienteMLabRaiz.get('', function(err, resM, body){
    if(err){
      console.log("ocurrrio un error", body);
    }else{
      res.send(body);
    }
  })
})

app.get('/Clientes', function(req,res){
  //var clienteMlab = requestJson.createClient(urlClientes);
  clienteMLabRaiz = requestJson.createClient(urlRaizMLab + "/Clientes?"+ apiKey);

  clienteMLabRaiz.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

app.get('/Clientes/:idcliente', function(req,res){
  //res.send("hola mundo");
  var idCliente = req.params.idcliente;
  console.log("consulta cliente", idCliente);

  clienteMLabRaiz = requestJson.createClient(urlRaizMLab + "/Clientes/"+idCliente+"/?"+ apiKey);

  clienteMLabRaiz.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

app.post('/Clientes', function(req,res){
  var nombre = req.body.nombre;
  var apePaterno = req.body.apePaterno;
  var apeMaterno = req.body.apeMaterno;
  var fechaNacimiento = req.body.fechaNacimiento;
  var celular = req.body.celular;


  clienteMLabRaiz = requestJson.createClient(urlRaizMLab + "/Clientes?"+ apiKey);

  var datos = {"nombre":nombre,
        "apeMaterno":apeMaterno,
        "apePaterno":apePaterno,
        "celular":celular,
        "fechaNacimiento":fechaNacimiento
    };

  var bodyString = JSON.stringify(datos);

  clienteMLabRaiz.post('', datos, function(err, resM, body) {
    if(!err){
      if(resM.statusCode == "200"){
        res.status(200).send("Cliente registrado");
      }else{
        res.status(404).send("Ups, no se pudo registrar al cliente");
      }
    }
  });
})

app.put('/Clientes/:idcliente', function(req,res){
  var idCliente = req.params.idcliente;
  var nombre = req.body.nombre;
  var apePaterno = req.body.apePaterno;
  var apeMaterno = req.body.apeMaterno;
  var fechaNacimiento = req.body.fechaNacimiento;
  var celular = req.body.celular;
  var listado = req.body.listado;

  console.log("actualiza cliente", idCliente);

  clienteMLabRaiz = requestJson.createClient(urlRaizMLab + "/Clientes/"+idCliente+"/?"+ apiKey);

  var datos = {"nombre":nombre,
        "apeMaterno":apeMaterno,
        "apePaterno":apePaterno,
        "celular":celular,
        "fechaNacimiento":fechaNacimiento,
        "listado":listado
    };

  var bodyString = JSON.stringify(datos);

  clienteMLabRaiz.put('', datos, function(err, resM, body) {
    if(!err){
      if(resM.statusCode == "200"){
        res.status(200).send("Cliente actualizado");
      }else{
        res.status(404).send("Ups, no se pudo actualizar al cliente");
      }
    }
  });

})

app.delete('/Clientes/:idcliente', function(req,res){
  var idCliente = req.params.idcliente;

  clienteMLabRaiz = requestJson.createClient(urlRaizMLab + "/Clientes/"+idCliente+"/?"+ apiKey);

  clienteMLabRaiz.del('', function(err, resM, body) {
    if(!err){
      if(resM.statusCode == "200"){
        res.status(200).send("Se ha eliminado el registro");
      }else{
        res.status(404).send("Ups, no se pudo eliminar el registro, intente de nuevo");
      }
    }
  });
})

app.get('/Usuarios', function(req,res){
  clienteMLabRaiz = requestJson.createClient(urlRaizMLab + "/Usuarios?"+ apiKey);

  clienteMLabRaiz.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

app.get('/Usuarios/:idcliente', function(req,res){
  var idCliente = req.params.idcliente;
  //console.log("consulta usuario", idCliente);

  clienteMLabRaiz = requestJson.createClient(urlRaizMLab + "/Usuarios/"+idCliente+"/?"+ apiKey);

  clienteMLabRaiz.get('', function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})

app.post('/Usuarios', function(req,res){
  var email = req.body.email;
  var password = req.body.password;

  clienteMLabRaiz = requestJson.createClient(urlRaizMLab + "/Usuarios?"+ apiKey);

  const encryptedValue = crypto.encrypt(password);
  //console.log("password encrip"+ encryptedValue);

  var datos = {"email":email,
        "password":encryptedValue
  };

  var bodyString = JSON.stringify(datos);

  clienteMLabRaiz.post('', datos, function(err, resM, body) {
    if(!err){
      if(resM.statusCode == "200"){
        res.status(200).send("Usuario registrado");
      }else{
        res.status(404).send("Ups, no se pudo registrar al usuario");
      }
    }
  });
})

app.put('/Usuarios/:idcliente', function(req,res){
  var idCliente = req.params.idcliente;
  var email = req.body.email;
  var password = req.body.password;

  console.log("actualiza cliente", idCliente);

  clienteMLabRaiz = requestJson.createClient(urlRaizMLab + "/Usuarios/"+idCliente+"/?"+ apiKey);

  const encryptedValue = crypto.encrypt(password);
  console.log("password encrip"+ encryptedValue);

  var datos = {"email":email,
        "password":encryptedValue
  };

  var bodyString = JSON.stringify(datos);

  clienteMLabRaiz.put('', datos, function(err, resM, body) {
    if(!err){
      if(resM.statusCode == "200"){
        res.status(200).send("Usuario actualizado");
      }else{
        res.status(404).send("Ups, no se pudo actualizar al usuario");
      }
    }
  });

})

app.delete('/Usuarios/:idcliente', function(req,res){
  var idCliente = req.params.idcliente;

  clienteMLabRaiz = requestJson.createClient(urlRaizMLab + "/Usuarios/"+idCliente+"/?"+ apiKey);

  clienteMLabRaiz.del('', function(err, resM, body) {
    if(!err){
      if(resM.statusCode == "200"){
        res.status(200).send("Se ha eliminado el registro");
      }else{
        res.status(404).send("Ups, no se pudo eliminar el registro, intente de nuevo");
      }
    }
  });
})

app.put('/', function(req,res){
  res.send("Hemos recibido su petcion put cambiada");
})

//para el login
app.post('/login', function(req,res){
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var email = req.body.email;
  var password = req.body.password;
  //var query = 'q={"email":"'+email+'","password":"'+password+'"}';
  var query = 'q={"email":"'+email+'"}';

  clienteMLabRaiz = requestJson.createClient(urlRaizMLab + "/Usuarios?"+ apiKey +"&"+ query);

  console.log(urlRaizMLab + "/Usuarios?"+ apiKey +"&"+ query);

  const encryptedValue = crypto.encrypt(password);
  console.log("password encrip"+ encryptedValue);

  /*
  //ejemplo encriptar
  const encryptedValue = crypto.encrypt(password);
  console.log("password encrip"+ encryptedValue);

  //ejemplo desencrip
  const decryptedValue = crypto.decrypt(encryptedValue);
  console.log("password desencrip"+ decryptedValue);
 */


  clienteMLabRaiz.get('', function(err, resM, body ){
    if(!err){
      if(body.length == 1){ //login ok
        /*Desencriptamos password y comparamos el valor recibido*/
        var passwordEncrip = body[0].password;

        const decryptedValue = crypto.decrypt(passwordEncrip);

        if(password == decryptedValue){
          console.log("los password son iguales");
          res.status(200).send("usuario logeado");
        }else{
          console.log("los password no coinciden")
          res.status(404).send("contraseña no valida");
        }


      }else{
        res.status(404).send("Usuario no encontrado, registrese");
      }
    }
  })


})
